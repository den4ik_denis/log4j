package com.DZlog4j;

import org.apache.log4j.Logger;

public class Random {

    private static final Logger log = Logger.getLogger(Random.class);

    public static int getRandomNumber() {
        return (int) (Math.random() * 10);
    }

    public static void getNumber() throws MyException {

        int a = getRandomNumber();
        if (a < 6) {
            log.error("Number generated - " + a);
            throw new MyException("Number generated - " + a);
        } else {
            log.info("Application launched successfully");
        }
    }
}
