package com.DZlog4j;

public class MyException extends Exception {

    public MyException(String message) {
        super(message);
    }
}
